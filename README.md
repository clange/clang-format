# clang-format container images

This repository holds the source for the container images used to run clang-format.

Available versions are:

- `v7`: clang-format version 7.0.1 (tags/RELEASE_701/final): `gitlab-registry.cern.ch/clange/clang-format:v7`
- `v16`: clang-format version 16.0.6 (Red Hat 16.0.6-1.el9): `gitlab-registry.cern.ch/clange/clang-format:v16`

## Usage examples

By using `-v $PWD:/code`, the code is mounted into the image from the current working directory into `/code` in the container.

To format the files in-place, add `-i` option.

Using `v7`:

```shell
docker run --rm -it --platform=linux/amd64 -v $PWD:/code gitlab-registry.cern.ch/clange/clang-format:v7 /code/src/CMSITminiDAQ.cc
```

Using `v16`:

```shell
docker run --rm -it --platform=linux/amd64 -v $PWD:/code gitlab-registry.cern.ch/clange/clang-format:v16 /code/src/CMSITminiDAQ.cc
```
